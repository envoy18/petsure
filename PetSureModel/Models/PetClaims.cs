﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetSureModel.Models
{
    public class PetClaims
    {
        public int Id { get; set; }

        public int PetId { get; set; }

        public Pets Pet { get; set; }

        public string Description { get; set; }

        public List<UploadedFiles> UploadedFiles { get; set; }

    }
}
