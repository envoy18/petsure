﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PetSureModel.Models
{
    public class UploadedFiles
    {
        public int Id { get; set; }

        public int PetClaimId { get; set; }

        public string FileName { get; set; }

        public string FilePath { get; set; }

        public int FileSize { get; set; }

        public string FileExtension { get; set; }
    }
}
