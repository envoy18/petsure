namespace PetSureModel.Context
{
    using PetSureModel.Models;
    using System;
    using System.Data.Entity;
    using System.Linq;
    using System.Reflection.Emit;

    public class PetSureDbContext : DbContext
    {
        public PetSureDbContext() : base("PetSureDbContext")
        {

        }

        public DbSet<Pets> Pets { get; set; }

        public DbSet<PetClaims> PetClaims { get; set; }

        public DbSet<UploadedFiles> UploadedFiles { get; set; }


    }
}