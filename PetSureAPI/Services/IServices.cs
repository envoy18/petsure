﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PetSureAPI.Services
{
    public interface IService<T> 
    {
        bool Add(T entity);

        bool Add(List<T> entities);

        bool Update(T updatedEntity);

        bool Delete(T entity);

        T Get(Guid guid);

        List<T> GetAll();
    }
}