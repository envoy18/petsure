﻿using PetSureModel.Context;
using PetSureModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PetSureAPI.Services
{
    public class PetSureServices : IService<Pets>
    {
        public static PetSureServices Instance { get { return new PetSureServices(); } }

        public bool Add(Pets entity)
        {
            return Add(new List<Pets> { entity });
        }

        public bool Add(List<Pets> entities)
        {
            using (var db = new PetSureDbContext())
            {
                db.Pets.AddRange(entities);
                db.SaveChanges();
            }

            return true;
        }

        public bool Delete(Pets entity)
        {
            throw new NotImplementedException();
        }

        public Pets Get(Guid guid)
        {
            throw new NotImplementedException();
        }

        public List<Pets> GetAll()
        {
            throw new NotImplementedException();
        }

        public List<Pets> GetPets()
        {
            using (var db = new PetSureDbContext())
            {
                return db.Pets.ToList();
            }
        }

        public bool Update(Pets updatedEntity)
        {
            throw new NotImplementedException();
        }
    }
}