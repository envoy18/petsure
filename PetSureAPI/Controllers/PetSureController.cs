﻿using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using PetSureAPI.Models;
using PetSureAPI.Services;
using PetSureModel.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Results;

namespace PetSureAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PetSureController : ApiController
    {

        [HttpPost]
        [Route("LoadPetList")]
        public JsonResult<List<Pets>> LoadPetList(GetPetModelParams angularParam)
        {
            var petList = PetSureServices.Instance.GetPets()
                                            .Where(x => x.OwnerId == angularParam.Id).ToList();
            
            return Json(petList);
        }

    }
}
